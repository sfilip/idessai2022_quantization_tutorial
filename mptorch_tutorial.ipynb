{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MPTorch Functionality Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook, we provide an overview of some of the main features of MPTorch. To install the current version of MPTorch, follow the instructions from the `README.md` in the GitHub [repo](https://github.com/mptorch/mptorch/blob/master/README.md)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import mptorch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Quantization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are currently three different types of number formats supported: floating-point, fixed-point, and block floating-point. Quantization functions for PyTorch tensors are provided for each format.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mptorch.quant import float_quantize, fixed_point_quantize, block_quantize"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Full Precision value: tensor([0.8469, 0.7699, 0.9222, 0.7366])\n",
      "Low Precision: tensor([0.8750, 0.7500, 0.8750, 0.7500])\n"
     ]
    }
   ],
   "source": [
    "full_prec_tensor = torch.rand(4)\n",
    "print(f\"Full Precision value: {full_prec_tensor}\")\n",
    "low_prec_tensor = float_quantize(\n",
    "  full_prec_tensor, exp=5, man=2, rounding='nearest'\n",
    ")\n",
    "print(f\"Low Precision: {low_prec_tensor}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Currently, nearest rounding and stochastic rounding are supported. The user can also specify if subnormal values are also allowed (yes by default)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Original: tensor([0.2961, 0.5166, 0.2517, 0.6886])\n",
      "Nearest: tensor([0.3125, 0.5000, 0.2500, 0.7500])\n",
      "Stochastic: tensor([0.3125, 0.5000, 0.2500, 0.6250])\n"
     ]
    }
   ],
   "source": [
    "torch.manual_seed(123)\n",
    "torch.cuda.manual_seed(123)\n",
    "torch.backends.cudnn.deterministic = True\n",
    "full_prec_tensor = torch.rand(4)\n",
    "nearest_round = float_quantize(full_prec_tensor, exp=5, man=2, rounding='nearest')\n",
    "stochastic_round = float_quantize(full_prec_tensor, exp=5, man=2, rounding='stochastic')\n",
    "print(f\"Original: {full_prec_tensor}\")\n",
    "print(f\"Nearest: {nearest_round}\")\n",
    "print(f\"Stochastic: {stochastic_round}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `saturate` flag that determines the behavior of the quantizer in case of overflows is also provided. If set to `True` quantized values will be set to the max/min representable value in the format (depending on the sign), otherwise they will be set to `Inf`. Propagating infinities can be useful in case of implementing/testing methods such as loss scaling, where scale update algorithms can work by detecting overflows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Original: tensor([-1.0240e+05,  4.0000e-01,  6.8053e+04])\n",
      "Saturated: tensor([-5.7344e+04,  3.7500e-01,  5.7344e+04])\n",
      "With Inf: tensor([  -inf, 0.3750,    inf])\n"
     ]
    }
   ],
   "source": [
    "x = torch.tensor([-102402.2, 0.4, 68053.3])\n",
    "qx_saturate = float_quantize(x, exp=5, man=2, rounding='nearest', saturate=True)\n",
    "qx_inf = float_quantize(x, exp=5, man=2, rounding='nearest', saturate=False)\n",
    "print(f\"Original: {x}\")\n",
    "print(f\"Saturated: {qx_saturate}\")\n",
    "print(f\"With Inf: {qx_inf}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To create a model for low precision training we can specify the various formats and quantizations to use for the layer operations and signals, in both the forward and the backward pass. For linear and convolutional layers (these are the only supported ones so far), the user can set a `QAffineFormats` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mptorch import FloatingPoint\n",
    "import mptorch.quant as qpt\n",
    "exp, man = 4, 2\n",
    "fp_format = FloatingPoint(exp=exp, man=man, subnormals=True, saturate=False)\n",
    "quant_fp = lambda x : qpt.float_quantize(\n",
    "  x, exp=exp, man=man, rounding=\"nearest\", subnormals=True, saturate=False\n",
    ")\n",
    "\n",
    "layer_formats = qpt.QAffineFormats(\n",
    "    fwd_add=fp_format,    # format to use for addition in FWD GEMM calls\n",
    "    fwd_mul=fp_format,    # format to use for multiplication in FWD GEMM calls\n",
    "    fwd_rnd=\"nearest\",    # rounding mode of the FWD mode operators\n",
    "    bwd_add=fp_format,    # format to use for addition in BWD GEMM calls\n",
    "    bwd_mul=fp_format,    # format to use for multiplication in BWD GEMM calls\n",
    "    bwd_rnd=\"nearest\",    # rounding mode of the BWD mode operators\n",
    "    param_quant=quant_fp, # how parameters should be quantized during FWD and BWD computations\n",
    "    input_quant=quant_fp, # how input signals should be quantized during FWD and BWD computations\n",
    "    grad_quant=quant_fp,  # how gradients should be quantized during FWD and BWD computations\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having defined the arithmetic configuration for forward and backward computations involving an affine layer (linear or convolution), we can now define a complete model. We will use the same MLP model that we saw in the Brevitas notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Reshape(torch.nn.Module):\n",
    "    def forward(self, x):\n",
    "        return x.view(-1, 28 * 28)\n",
    "\n",
    "\n",
    "model = torch.nn.Sequential(\n",
    "    Reshape(),\n",
    "    qpt.QLinear(784, 128, formats=layer_formats),\n",
    "    torch.nn.ReLU(),\n",
    "    qpt.QLinear(128, 96, formats=layer_formats),\n",
    "    torch.nn.ReLU(),\n",
    "    qpt.QLinear(96, 10, formats=layer_formats),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A mixed-precision training example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are almost ready to simulate a mixed-precision training workflow, but there are still a couple of things to do. We need to load the data for training (in our case MNIST), set the precision to use during the parameter update process (this is achieved through a `mptorch.optim.MPOptim` wrapper to standard PyTorch optimizers), choose our training (hyper)parameters and that's about it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Epoch 0, lr 0.050: 100%|██████████| 60032/60032 [00:27<00:00, 2215.39it/s, train_acc=0.86933, train_loss=0.46680]\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss 0.467, train acc 0.869, test acc 0.916\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Epoch 1, lr 0.050: 100%|██████████| 60032/60032 [00:26<00:00, 2273.70it/s, train_acc=0.92990, train_loss=0.27570]\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss 0.276, train acc 0.930, test acc 0.936\n"
     ]
    }
   ],
   "source": [
    "from torchvision import datasets, transforms\n",
    "from torch.utils.data import DataLoader\n",
    "from torch.optim import SGD\n",
    "from mptorch.optim import OptimMP\n",
    "from mptorch.utils import trainer\n",
    "\n",
    "\"\"\"Hyperparameters\"\"\"\n",
    "batch_size = 64  # batch size\n",
    "lr_init = 0.05  # initial learning rate\n",
    "num_epochs = 2  # epochs\n",
    "momentum = 0.9\n",
    "weight_decay = 0\n",
    "\n",
    "\"\"\"Prepare the transforms on the dataset\"\"\"\n",
    "device = \"cuda\" if torch.cuda.is_available() else \"cpu\"\n",
    "transform = transforms.Compose(\n",
    "    [\n",
    "        transforms.ToTensor(),\n",
    "        transforms.Normalize((0.1307,), (0.3081,)),\n",
    "    ]\n",
    ")\n",
    "\n",
    "\"\"\"download dataset: MNIST\"\"\"\n",
    "train_dataset = datasets.MNIST(\n",
    "    \"./data\", train=True, transform=transform, download=True\n",
    ")\n",
    "train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)\n",
    "test_dataset = datasets.MNIST(\n",
    "    \"./data\", train=False, transform=transform, download=False\n",
    ")\n",
    "test_loader = DataLoader(test_dataset, batch_size=int(batch_size), shuffle=False)\n",
    "\n",
    "\"\"\"Prepare and launch the training process\"\"\"\n",
    "model = model.to(device)\n",
    "optimizer = SGD(\n",
    "    model.parameters(), lr=lr_init, momentum=momentum, weight_decay=weight_decay\n",
    ")\n",
    "\n",
    "# choose the precision for the parameter update process (here it is full FP32 precision)\n",
    "acc_q = lambda x: qpt.float_quantize(x, exp=8, man=23, rounding=\"nearest\")\n",
    "optimizer = OptimMP(\n",
    "    optimizer,\n",
    "    acc_quant=acc_q,\n",
    "    momentum_quant=acc_q,\n",
    ")\n",
    "scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_epochs)\n",
    "\n",
    "trainer(\n",
    "    model,\n",
    "    train_loader,\n",
    "    test_loader,\n",
    "    num_epochs=num_epochs,\n",
    "    lr=lr_init,\n",
    "    batch_size=batch_size,\n",
    "    optimizer=optimizer,\n",
    "    device=device,\n",
    "    init_scale=1024.0,\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9.12 ('base')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "d9d8a3f6a7eec910353d3dc87f6184bad6ff25cfe3b16a22ecf93706cdb4d06b"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
